# docker-wordpress

Quickstart, cause we're lazy:
* Images published on gitlab: https://gitlab.com/kordianbruck/docker-wordpress/container_registry

Simply deploy the helm chart:
```bash
minikube start
cd helm
./deploy.sh
# Wait till mysql is running and ready
# Delete the wordpress deployment & recreate, so we can properly init the db
kubectl delete -n amazing-blog deployment/wordpress
./deploy.sh
```

Get your local address from minikube:
```bash
$ minikube status
minikube: Running
cluster: Running
kubectl: Correctly Configured: pointing to minikube-vm at 192.168.99.101
```

Open your browser pointing to http://192.168.99.101:30000
(Exposed via nodePort, cause I couldn't get `minikcube service` to work)