# Wordpress docker

A not so smart way to run wordpress...

## Improvements

### Separate apache/php containers
We really just want to run one process per container. The way 
wordpress functions makes this especially difficult, as it 
not really stateless. 

Static files are mixed in with dynamic 
content for lack of a real separation between themes and wordpress
itself. A lot of functionality for more complex sites are usually
implemented (wrongly) in themes, so we end up in this situation,
where we have to run an additional reverse proxy in front of the 
fpm server to handle static content. 

Ideally we could optimize this to use nginx instead of apache and
eventually really just use themes with static content. (different cms?)

### Redundancy & HA
As it is, we currently only have one pod for the database and the frontend. 

For the database, we would need to setup a cluster and/or read replicas etc. 
This seems like overkill for the purpose of this exercise. 

The frontend is trickier. We would need to share the files that
can change between the pods. (Uploads mainly, Sessions?) See above...

### Secrets & DB initialization
Currently all the database secrets are hard coded as for time considerations.
Ideally we would store these inside k8s secrets and pass them in through
environment variables. This would be less error prone and more secure,
as we could utilize random passwords instead of the current static ones.

Database seeding (creating the db) could be done from within wordpress,
instead of installing the mysql-client and creating the db at run time. 
This would make the image smaller and remove one more dependency.

This currently also is a real problem, as we need to actually wait till the db
is ready in order to start the wordpress image in order to be able to create 
the database. Kubernetes does not have a "dependson" syntax, so our current
"workaround" solution is not the cream de la cream.

### Persistent Volumes for wordpress
All uploads to wordpress will currently be discarded, as we have
not mapped folders that are used for uploads. With a production system
it would be smart to use a real store system like s3buckets or similar,
so apps don't have to deal with file access/volume mapping. 

=> Stateless scales better

### Ingress Controller & Cert-manager
For any decent cluster, we would have a nice ingress controller and
a cert-manager to get tls certificates. This is out of scope for this PoC.

### Deployment via gitlab ci
The pipeline could be extended to deploy any changes to a staging env and
merged changes to production to further extend our automation. This
is somewhat tricky, as you have to figure out a way to access the cluster
from the gitlab runner, which might not be as easy security wise.

### Security
Yea, good idea. Not really considered in this PoC. From using public images, to
hard coding passwords, to using the db root user for wordpress. All of those are
rather not so good best practices, but for simplicities sake, we will tolerate this.

Ideally we would build our own php app image, without relying on big clunky php-apache
images. We can then control better the dependencies. 
Also we should probably download fixed versions of wordpress, instead of latest.
