#!/bin/bash
set -e
TEMPLATE_FILE=./compiled_template.yaml
NAMESPACE=amazing-blog

helm template --values values.yaml . > "${TEMPLATE_FILE}"
kubectl apply -f ${TEMPLATE_FILE}

if [ -t 0 ] && [ -t 1 ]; then
    watch -n1 "kubectl get pods,svc,ingress,deployment -n ${NAMESPACE}"
fi